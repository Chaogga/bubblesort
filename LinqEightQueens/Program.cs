﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

class Program
{
    static void Main(string[] args)
    {
        doBubbleSort();

        Console.ReadKey();
    }
    private static void doBubbleSort()
    {
        string text = $@"SOCO Machinery Co.,Ltd.No.3, Jingke E. Rd.Nantun Dist., Taichung City 40852 Taiwan +886-4 2359-1888";
        //去空白
        text = text.Replace(" ", "");
        //去除特殊字元
        text = Regex.Replace(text, @"[\W_]+", "");
        //去除重複且將字串轉換ascii-code
        List<int> NumberList = new List<int>();
        IEnumerable<char> distinctList = text.Distinct();
        foreach (char a in distinctList)
        {
            if (!NumberList.Contains((int)a))
                NumberList.Add((int)a);
        }
        //開始做氣泡排序
        int temp = 0;
        string result = "";
        //用ascii-code做排序(數字先排後排大寫英文最後排小寫英文)
        for (int i = 0; i < NumberList.Count; i++)
        {
            for (int k = i + 1; k < NumberList.Count; k++)
            {
                if (NumberList[i] > NumberList[k])
                {
                    temp = NumberList[k];
                    NumberList[k] = NumberList[i];
                    NumberList[i] = temp;
                }
            }
        }
        //將ascii-code轉回數字及英文並且組成字串印出
        NumberList.ForEach(x =>
        {
            System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
            byte[] a = new byte[] { (byte)x };
            result += asciiEncoding.GetString(a);
        });
        Console.WriteLine($"{result}");
    }
}

